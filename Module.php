<?php

namespace backend\modules\Parser;

/**
 * parser module definition class
 */
class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\Parser\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
