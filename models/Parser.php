<?php

namespace backend\modules\Parser\models;

use backend\models\tables\Calendary;
use backend\models\tables\Comp;
use backend\models\tables\CompPs;
use backend\models\tables\CompPsHasComp;
use backend\models\tables\Dc;
use backend\models\tables\Disciplins;
use backend\models\tables\Fgos;
use backend\models\tables\Fo;
use backend\models\tables\Form;
use backend\models\tables\Institut;
use backend\models\tables\KafHasComp;
use backend\models\tables\Kurs;
use backend\models\tables\Kvalification;
use backend\models\tables\MainPlan;
use backend\models\tables\Np;
use backend\models\tables\Podpisants;
use backend\models\tables\ProfStandart;
use backend\models\tables\Session;
use backend\models\tables\SpravDis;
use backend\models\tables\SpravFacultet;
use backend\models\tables\SpravKafedra;
use backend\models\tables\SpravUchGod;
use backend\models\tables\SrocEducation;
use backend\models\tables\Staff;
use backend\models\tables\TypeForm;
use backend\models\tables\TypePeriods;
use backend\models\tables\TypeSession;
use backend\models\tables\TypeTaskPd;
use backend\models\tables\TypeWork;
use backend\models\tables\UpProfiles;
use common\models\ImportFile;
use Yii;
use yii\base\Model;

class Parser extends Model {
    public $file;
    private $xml;
    private $not_parse_errors = true;
    private $main_plan_id; // id УП, который загружается
    private $is_double = false; // загружается ли повторяющийся учебный план
    private $archive_main_plan_id; // id старого УП, который совпал с новым

    /**
     * @author kirsanov_av
     * @date 2021-08-20 19:38
     * @inheritdoc Сюда сохраняются данные об уже выполненных парсингах
     * @var array
     */
    private $dataParse = array();

    public function check_extension() {
        // возвращает true, если расширение файла находится в списке разрешённых
        $allow_extensions = ['plx', 'xml', 'txt'];
        $file_name  = explode('.', $this->file->name);
        $file_extension = $file_name[count($file_name) - 1];

        foreach ($allow_extensions as $extension) {
            if ($extension == $file_extension) {
                return true;
            }
        }
        return false;
    }

    private function parse_fgos_table() {
        $table = new Fgos();
        $path = eval(Paths::$path_to_Планы);

        $number = (string) $path['НомерФГОС'];
        if (Fgos::find()->andWhere(['number' => $number])->count() == 1) {
            $table = Fgos::findOne(['number' => $number]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->number = $number;
            $table->date = $path['ДатаГОСа'];
            $table->path_file = 'null';

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }
    }

    public function upload() { // сохранение файлов в папку import-plan
        //1. сгенерировать имя файла
        $name = $this->genNewName();

        //2. сохранить файл в папку
        $this->file->saveAs(__DIR__ . '/../../../../storage/web/source/import-plan/' . $name);

        //3. сохранить путь к файлу в бд
        $customer = new ImportFile();
        $customer->url_file = '/source/import-plan/' . $name;
        $customer->name_file = $this->file->name;
        $customer->main_plan_id = $this->main_plan_id;

        $customer->created_at = time();
        $customer->created_by = Yii::$app->user->getId();
        $customer->updated_at = time();
        $customer->updated_by = Yii::$app->user->getId();
        $customer->active = 1;
        $customer->lock = 1;

        $customer->save();

        return true;
    }

    private function genNewName() {
        $rshren = $this->file->name;
        $extension = explode(".", $rshren);


        $resultName = uniqid() . '.' . $extension[count($extension)-1];
        return $resultName;
    }


    private function parse_comp_table() {
        $path = eval(Paths::$path_to_ПланыКомпетенции);

        foreach ($path as $p) {

            $index = (string) $p['ШифрКомпетенции'];

            $table = new Comp();

            $table->index = $index;
            $table->soderzhanie = $p['Наименование'];
            $table->main_plan_id = $this->main_plan_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_institut_table() {
        $path = eval(Paths::$path_to_Филиалы);

        foreach ($path as $p) {
            $table = new Institut();

            $name = (string) $p['Полное_название'];
            $code_filiala = (string) $p['Код_филиала'];
            if (Institut::find()->andWhere(['name' => $name, 'code_filiala' => $code_filiala])->count() == 1) {
                $table = Institut::findOne(['name' => $name, 'code_filiala' => $code_filiala]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->name = $name;
                $table->code_filiala = $code_filiala;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_type_task_pd_table() {
        $table = new TypeTaskPd();

        $path = eval(Paths::$path_to_ВидыДеятельности);
        $path_pvd = eval(Paths::$path_to_ПланыВидыДеятельности);

        $np_code = (string) $path_pvd['КодВидаДеятельности'];
        $name = '';
        foreach ($path as $p) {
            if (((string) $p['Код']) == $np_code) {
                $name = (string) $p['Наименование'];
                break;
            }
        }

        if (TypeTaskPd::find()->andWhere(['name' => $name])->count() == 1) {
            $table = TypeTaskPd::findOne(['name' => $name]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }
    }

    private function parse_sprav_facultet_table() {
        $path = eval(Paths::$path_to_Факультеты);

        foreach ($path as $p) {
            $table = new SpravFacultet();

            $name = (string) $p['Факультет'];
            $filial_code = (int) $p['Код_филиала'];
            $institut_id = (Institut::findOne(['code_filiala' => $filial_code]))->id;

            if (SpravFacultet::find()->andWhere(['name' => $name, 'institut_id' => $institut_id])->count() == 1) {
                $table = SpravFacultet::findOne(['name' => $name, 'institut_id' => $institut_id]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->name = $name;
                $table->institut_id = $institut_id;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_kvalification_table() {
        $path = eval(Paths::$path_to_Планы);
        $table = new Kvalification();

        $name = (string) $path['Квалификация'];
        if (Kvalification::find()->andWhere(['name' => $name])->count() == 1) {
            $table = Kvalification::findOne(['name' => $name]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }
    }

    private function parse_fo_table() {
        $path = eval(Paths::$path_to_ФормаОбучения);

        foreach ($path as $p){
            $table = new Fo();

            $name = (string) $p['ФормаОбучения'];
            if (Fo::find()->andWhere(['name' => $name])->count() == 1) {
                $table = Fo::findOne(['name' => $name]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->name = $name;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_type_work_table() {
        $path = eval(Paths::$path_to_СправочникВидыРабот);

        foreach ($path as $p){
            $table = new TypeWork();

            $name = (string) $p['Название'];

            // если такой вид работ есть, то пропускаем
            if (TypeWork::findOne(['name' => $name]) != null) {
                continue;
            }

            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_type_periods_table() {
        $path = eval(Paths::$path_to_ПланыГрафикиЯчейки);

        foreach ($path as $p){
            $table = new TypePeriods();

            $name = (string) $p['КодВидаДеятельности'];

            // если такой период существует, то пропускаем
            if (TypePeriods::findOne(['name' => $name])) {
                continue;
            }

            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }

        }
    }

    private function parse_staff_table() {
        $path = eval(Paths::$path_to_ДолжностныеЛица);

        foreach ($path as $p){
            $table = new Staff();

            $FIO = (string) $p['ФИО'];
            $F = explode(" ", $FIO);
            $I = explode (".", $F[1]);
            $O = explode (".", $F[1]);

            $post = (string) $p['Должность'];
            if (Staff::find()->andWhere(['f' => $F[0], 'i' => $I[0], 'o' => $O[1], 'post' => $post])->count() == 1) {
                $table = Staff::findOne(['f' => $F[0], 'i' => $I[0], 'o' => $O[1], 'post' => $post]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->f = $F[0];
                $table->i = $I[0];
                $table->o = $O[1];
                $table->post = $post;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_sroc_education_table() {
        $path = eval(Paths::$path_to_Планы);

        $table = new SrocEducation();

        $god = (string) $path['СрокОбучения'];
        $mesec = (string) $path['СрокОбученияМесяцев'];
        $name = $god.'г '.$mesec.'м';
        if (SrocEducation::find()->andWhere(['name' => $name])->count() == 1) {
            $table = SrocEducation::findOne(['name' => $name]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }
    }

    private function parse_prof_standart_table() {
        $path = eval(Paths::$path_to_псСтандарты);
        if ($path['ПриказДата'] == null) { return; }
        $table = new ProfStandart();

        $number = (string) $path['НомерВГруппе'];
        if (ProfStandart::find()->andWhere(['number' => $number])->count() == 1) {
            $table = ProfStandart::findOne(['number' => $number]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->number = $number;
            $table->date = $path['ПриказДата'];

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }
    }

    private function parse_comp_ps_table() {
        $path = eval(Paths::$path_to_псГруппы);
        if ($path['Группа'] == null) { return; }

        $standart = eval(Paths::$path_to_псСтандарты);
        $standart_number = (string) $standart['НомерВГруппе'];
        $standart_id = (ProfStandart::findOne(['number' => $standart_number]))->id;

        $path = eval(Paths::$path_to_псГруппы);
        $table = new CompPs();
        $index = (string) $path['КодГруппы'];
        $name = (string) $path['Группа'];
        if (CompPs::find()->andWhere(['name' => $name, 'index' => $index])->count() == 1) {
            $table = CompPs::findOne(['name' => $name, 'index' => $index]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        }
        else {
            $table->index = $index;
            $table->name = $name;
            $table->prof_standart_id = $standart_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }
        if (!$table->save()) {
            $this->not_parse_errors = false;
        }

        $path = eval(Paths::$path_to_псСтандарты);
        $table = new CompPs();
        $number_in_group = (string) $path['НомерВГруппе'];
        $name = (string) $path['НаименованиеСтандарта'];
        if (CompPs::find()->andWhere(['name' => $name, 'index' => $number_in_group])->count() == 1) {
            $table = CompPs::findOne(['name' => $name, 'index' => $number_in_group]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->index = $number_in_group;
            $table->name = $name;
            $table->prof_standart_id = $standart_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }

        $path = eval(Paths::$path_to_псОбобщенныеФункции);
        foreach ($path as $p) {
            $table = new CompPs();

            $name = (string) $p['ОбобщеннаяФункция'];
            $index = (string) $p['Шифр'];
            $parent_id = (CompPs::findOne(['index' => $number_in_group]))->id;
            if (CompPs::find()->andWhere(['name' => $name, 'index' => $index])->count() == 1) {
                $table = CompPs::findOne(['name' => $name, 'index' => $index]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->name = $name;
                $table->index = $index;
                $table->trebovanie = $p['ТребованияОбразование'];
                $table->prof_standart_id = $standart_id;
                $table->parent_id = $parent_id;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }

        $path = eval(Paths::$path_to_псФункции);
        foreach ($path as $p) {
            $table = new CompPs();

            $name = (string) $p['Функция'];
            $index = (string) $p['Шифр'];
            $parent_id = (CompPs::findOne(['index' => $index[0]]))->id;
            if (CompPs::find()->andWhere(['name' => $name, 'index' => $index])->count() == 1) {
                $table = CompPs::findOne(['name' => $name, 'index' => $index]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->name = $name;
                $table->index = $index;
                $table->prof_standart_id = $standart_id;
                $table->parent_id = $parent_id;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }

    }

    private function parse_type_session_table() {
        $path = eval(Paths::$path_to_Заезды);

        foreach ($path as $p){
            $table = new TypeSession();

            $name = (string) $p['Название'];

            // если такой тип, то пропускаем
            if (TypeSession::findOne(['name' => $name])) {
                continue;
            }

            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }

        $number_semestrs = 2;
        for ($i = 1; $i <= $number_semestrs; $i++) {
            $table = new TypeSession();

            $name = 'Семестр ' . $i;
            if (TypeSession::find()->andWhere(['name' => $name])->count() == 1){
                $table = TypeSession::findOne(['name' => $name]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table->name = $name;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_form_table() {
        $path_object_types = eval(Paths::$path_to_СправочникТипОбъекта);
        $path_dis = eval(Paths::$path_to_ПланыСтроки);

        foreach ($path_object_types as $type_object) {
            $data = (string) $type_object['Код'];

            $name_form = (string) $type_object['Название'];
            $type_form_id = (TypeForm::findOne(['name' => $name_form]))->id;

            foreach ($path_dis as $pd){
                $disciplins = (string) $pd['ДисциплинаКод'];
                $disciplins_id = (Disciplins::findOne(['index' => $disciplins, 'main_plan_id'=>$this->main_plan_id]))->id;

                $table = new Form();
                $table->value = $data;
                $table->type_form_id = $type_form_id;
                $table->disciplins_id = $disciplins_id;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;

                if (!$table->save()) {
                    $this->not_parse_errors = false;
                }
            }

        }
    }

    private function parse_sprav_kafedra_table() {
        $path = eval(Paths::$path_to_Кафедры);
        $path1 = eval(Paths::$path_to_Факультеты);

        foreach ($path as $p) {
            $table = new SpravKafedra();

            $name = (string)$p['Название'];


            if (SpravKafedra::find()->andWhere(['name' => $name])->count() == 1) {
                continue;
            }
            $table->name = $name;
            foreach ($path1 as $p1) {
                $facultet = (string) $p1['Факультет'];
                $facultet_id = (SpravFacultet::findOne(['name' => $facultet]))->id;
                if (SpravKafedra::find()->andWhere(['name' => $name, 'sprav_facultet_id' => $facultet_id])->count() == 1) {
                    $table = SpravKafedra::findOne(['name' => $name, 'sprav_facultet_id' => $facultet_id]);
                    $table->updated_at = time();
                    $table->updated_by = Yii::$app->user->getId();
                } else {
                    $table->sprav_facultet_id = $facultet_id;

                    $table->created_at = time();
                    $table->created_by = Yii::$app->user->getId();
                    $table->updated_at = time();
                    $table->updated_by = Yii::$app->user->getId();
                    $table->active = 1;
                    $table->lock = 1;
                }

                if (!$table->save()) {
                    $this->not_parse_errors = false;
                }
            }
        }
    }

    private function parse_sprav_dis_table(){
        $path = eval(Paths::$path_to_ПланыСтроки);
        $path1 = eval(Paths::$path_to_Кафедры);

        foreach ($path as $p) {
            $table = new SpravDis();

            $name = (string)$p['Дисциплина'];


            if (SpravDis::find()->andWhere(['name' => $name])->count() == 1) {
                continue;
            }
            $table->name = $name;
            foreach ($path1 as $p1) {
                $kafedra = (string) $p1['Название'];
                $kafedra_id = (SpravKafedra::findOne(['name' => $kafedra]))->id;

                $table->sprav_kafedra_id = $kafedra_id;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;


                if (!$table->save()) {
                    $this->not_parse_errors = false;
                }
            }
        }
    }

    private function parse_type_form_table(){
        $path = eval(Paths::$path_to_СправочникТипОбъекта);

        foreach ($path as $p) {
            $table = new TypeForm();

            $name = (string) $p['Название'];

            // если такой тип существует, то пропускаем
            if (TypeWork::findOne(['name' => $name])) {
                continue;
            }

            $table->name = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function find_parent_in_disciplins($parent_id) {
        $path_dis = eval(Paths::$path_to_ПланыСтроки);

        $parent_index = '';
        foreach ($path_dis as $dis) {
            if ((integer) $dis['Код'] == $parent_id) {
                $parent_index = (string) $dis['ДисциплинаКод'];
            }
        }

        if ($parent_index !== '') {
            $dis = Disciplins::findOne(['index' => $parent_index, 'main_plan_id'=>$this->main_plan_id]);
            if (is_object($dis)){
                return $dis->id;
            }

        }
        return null;
    }

    private function parse_disciplins_table(){
        $path_kaders = eval(Paths::$path_to_Кафедры);
        $path_plans = eval(Paths::$path_to_Планы);
        $path_dis = eval(Paths::$path_to_ПланыСтроки);

        $kafedra_code = (string) $path_plans['КодПрофКафедры'];
        $kafedra_name = '';
        foreach ($path_kaders as $kafedra) {
            if (((string) $kafedra['Код']) == $kafedra_code) {
                $kafedra_name = (string) $kafedra['Название'];
                break;
            }
        }
        $kafedra_id = (SpravKafedra::findOne(['name' => $kafedra_name]))->id;

        $batchArray = array();
        $batchField = array(
            'index',
            'parent_id',
            'sprav_dis_id',
            'sprav_kafedra_id',
            'main_plan_id',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'deleted_by',
            'active',
            'lock',
        );
        foreach ($path_dis as $pd) {
//            $table = new Disciplins();
            $temp = array();
            $index = (string) $pd['ДисциплинаКод'];
            $parent_id = $this->find_parent_in_disciplins((integer) $pd['КодРодителя']);
            $sprav_dis = (string) $pd['Дисциплина'];
            $sprav_dis_id = (SpravDis::findOne(['name' => $sprav_dis]))->id;

            $temp[] = $index;
            $temp[] = $parent_id;
            $temp[] = $sprav_dis_id;
            $temp[] = $kafedra_id;
            $temp[] = $this->main_plan_id; //main_plan_id
            $temp[] = time(); //created_at
            $temp[] = Yii::$app->user->getId(); //created_by
            $temp[] = time(); //updated_at
            $temp[] = Yii::$app->user->getId(); //updated_by
            $temp[] = 0; //deleted_by
            $temp[] = 1; //active
            $temp[] = 1; //lock
            $batchArray[] = $temp;
        }

        if (!Yii::$app->db->createCommand()->batchInsert('disciplins',$batchField,$batchArray)->execute()) {
            Yii::info('Дисциплины не добавлены.');
            $this->not_parse_errors = false;
        }
    }

    private function parse_kaf_has_comp_table() {
        $path_k = eval(Paths::$path_to_Кафедры);
        $path_c = eval(Paths::$path_to_ПланыКомпетенции);

        foreach ($path_k as $pk) {
            $kafedra = (string) $pk['Название'];
            $kafedra_id = (SpravKafedra::findOne(['name' => $kafedra]))->id;

            foreach ($path_c as $pc) {
                $table = new KafHasComp();
                $comp = (string) $pc['ШифрКомпетенции'];
                $comp_id = (Comp::findOne(['index' => $comp]))->id;

                $table->sprav_kafedra_id = $kafedra_id;
                $table->comp_id = $comp_id;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;

                if (!$table->save()) {
                    $this->not_parse_errors = false;
                }
            }
        }
    }

    private function parse_np_table() {
        $table = new Np();

        $path_oop = eval(Paths::$path_to_ООП);
        $path_activites = eval(Paths::$path_to_ВидыДеятельности);
        $path_ps_standarts = eval(Paths::$path_to_псСтандарты);
        $path_pvd = eval(Paths::$path_to_ПланыВидыДеятельности);

        $code =  (string) $path_oop['Шифр'];
        $name = (string) $path_oop['Название'];

        $ps_name = (string) $path_ps_standarts['НаименованиеСтандарта'];
        $comp_ps_id = '';
        if (!($ps_name == '')) {
            $comp_ps_id = (CompPs::findOne(['name' => $ps_name]))->id;
        }

        $vd_code = (string) $path_pvd['КодВидаДеятельности'];
        $vd_name = '';
        foreach ($path_activites as $active) {
            if (((string) $active['Код']) == $vd_code) {
                $vd_name = (string) $active['Наименование'];
                break;
            }
        }
        $type_task_pd_id = (TypeTaskPd::findOne(['name' => $vd_name]))->id;

        if (Np::find()->andWhere(['code' => $code, 'name' => $name, 'type_task_pd_id' => $type_task_pd_id])->count() == 1) {
            $table = Np::findOne(['code' => $code, 'name' => $name, 'type_task_pd_id' => $type_task_pd_id]);
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
        } else {
            $table->name = $name;
            $table->code = $code;
            $table->comp_ps_id = $comp_ps_id;
            $table->type_task_pd_id = $type_task_pd_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;
        }

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }

    }

    private function parse_sprav_uch_god_table(){
        $path = eval(Paths::$path_to_ООП);
        $path1 = eval(Paths::$path_to_Планы);

        foreach ($path as $p) {
            $name = (string) $p['Название'];
        }

        foreach ($path1 as $p1){
            $uch_god = (string) $p1['УчебныйГод'];
            $years = explode("-", $uch_god);
        }

        foreach ($path1 as $p1){
            $table = new SpravUchGod();

            if (SpravUchGod::find()->andWhere(['year_end' => $years[1], 'name' => $uch_god, 'year_begin' => $years[0]])->count() == 1){
                $table = SpravUchGod::findOne(['year_end' => $years[1], 'name' => $uch_god, 'year_begin' => $years[0]]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
//                $table->name = $name;
                $table->name = $uch_god;
                $table->year_begin = $years[0];
                $table->year_end = $years[1];

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 1;
            }

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
            else{
                $this->dataParse['sprav_uch_god_id'] = $table->id;
            }
        }
    }

    private function parse_kurs_table(){
        $path = eval(Paths::$path_to_Планы);

        $number_courses = (string) $path['ЧислоКурсов'];
        for ($course = 1; $course <= (integer) $number_courses; $course++) {
            $table = new Kurs();

            $name = $course;

            // если такой курс есть, то пропускаем
            if (Kurs::findOne(['number_kurs' => $name])) {
                continue;
            }

            $table->number_kurs = $name;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_main_plan_table() {
        $table = new MainPlan();

        $path_plans = eval(Paths::$path_to_Планы);
        $path_parametrs_plan = eval(Paths::$path_to_ПараметрыПлана);
        $path_kafedrs = eval(Paths::$path_to_Кафедры);
        $path_oop = eval(Paths::$path_to_ООП);
        $path_fo = eval(Paths::$path_to_ФормаОбучения);
        $path_dl_plans = eval(Paths::$path_to_ДолжЛица_Планы);
        $path_dl = eval(Paths::$path_to_ДолжностныеЛица);

        $date_sp = (string) $path_plans['ГодНачалаПодготовки'];
        $date_ut = (string) $path_plans['ДатаУтверСоветом'];

        $organization_code = (string) $path_plans['КодОрганизации'];
        $name_or = (Institut::findOne(['code_filiala' => $organization_code]))->name;
        $name_ministry = (string) $path_parametrs_plan['RUPMinistry'];
        $protocol_number = (string) $path_plans['НомПротокСовета'];
        $protocol_date = (string) $path_plans['ДатаУтверСоветом'];

        $fgos_number = (string) $path_plans['НомерФГОС'];
        $fgos_id = (Fgos::findOne(['number' => $fgos_number]))->id;

        $code_kafedra = (string) $path_plans['КодПрофКафедры'];
        $name_kafedra = '';
        foreach ($path_kafedrs as $kafedra) {
            if (((string) $kafedra['Код']) == $code_kafedra) {
                $name_kafedra = (string) $kafedra['Название'];
                break;
            }
        }
        $sprav_kafedra_id = (SpravKafedra::findOne(['name' => $name_kafedra]))->id;

        $name_kvalification = (string) $path_plans['Квалификация'];
        $kvalification_id = (Kvalification::findOne(['name' => $name_kvalification]))->id;


        $code_fo = (string) $path_plans['КодФормыОбучения'];
        $name_fo = '';
        foreach ($path_fo as $fo) {
            if (((string) $fo['Код']) == $code_fo) {
                $name_fo = (string) $fo['ФормаОбучения'];
                break;
            }
        }
        $fo_id = (Fo::findOne(['name' => $name_fo]))->id;

        $do_code = '';
        foreach ($path_dl_plans as $dl) {
            if (((string) $dl['Утвердил']) == 'true') {
                $do_code = (string) $dl['КодДолжЛица'];
                break;
            }
        }
        $dl_f = ''; $dl_n = ''; $dl_o = '';
        foreach ($path_dl as $dl) {
            if (((string) $dl['Код']) == $do_code) {
                $fio = explode(' ', (string) $dl['ФИО']);
                $dl_f = $fio[0];
                $dl_n = explode('.', $fio[1])[0];
                $dl_o = explode('.', $fio[1])[1];
                break;
            }
        }
        $staff_id = (Staff::findOne(['f' => $dl_f, 'i' => $dl_n, 'o' => $dl_o]))->id;

        $training_period = ((string) $path_plans['СрокОбучения']) . 'г ' . ((string) $path_plans['СрокОбученияМесяцев']) . 'м';
        $sroc_education_id = (SrocEducation::findOne(['name' => $training_period]))->id;

        $years = explode('-', (string) $path_plans['УчебныйГод']);
        $year_begin = $years[0];
        $year_end = $years[1];
//        $facultet_name = (string) $path_oop['Название'];
        $sprav_uch_god_id = array_key_exists('sprav_uch_god_id',$this->dataParse)
            ? $this->dataParse['sprav_uch_god_id']
            :(SpravUchGod::findOne(['year_begin' => $year_begin,'year_end' => $year_end]))->id;

        $np_name = (string) $path_oop['Название'];
        $np_id = (Np::findOne(['name' => $np_name]))->id;
        $up_profiles_id = array_key_exists('up_profiles_id',$this->dataParse) ? $this->dataParse['up_profiles_id'] : null;


        $old_main_plan = MainPlan::findOne([
            'date_sp' => $date_sp,
            'date_ut' => $date_ut,
            'name_or' => $name_or,
//            'name_ministry' => $name_ministry,
            'n_protocol' => $protocol_number,
            'date_protocol' => $protocol_date,
            'fgos_id' => $fgos_id,
            'sprav_kafedra_id' => $sprav_kafedra_id,
            'kvalification_id' => $kvalification_id,
            'np_id' => $np_id,
            'fo_id' => $fo_id,
            'sroc_education_id' => $sroc_education_id,
            'sprav_uch_god_id' => $sprav_uch_god_id,
            'up_profiles_id' => $up_profiles_id,
        ]);
        // если такой учебный план существует, то делаем его архивным
        if ($old_main_plan != null) {
            $old_main_plan->archive = true;
            $this->archive_main_plan_id = $old_main_plan->id;
            $this->is_double = true;

            if (!$old_main_plan->save()) {
                $this->not_parse_errors = false;
            }
        }

        // добавляем новые данные по УП
        $table->date_sp = $date_sp;
        $table->date_ut = $date_ut;
        $table->name_or = $name_or;
        $table->name_ministry = $name_ministry;
        $table->n_protocol = $protocol_number;
        $table->date_protocol = $protocol_date;
        $table->fgos_id = $fgos_id;
        $table->sprav_kafedra_id = $sprav_kafedra_id;
        $table->kvalification_id = $kvalification_id;
        $table->np_id = $np_id;
        $table->fo_id = $fo_id;
        $table->staff_id = $staff_id;
        $table->sroc_education_id = $sroc_education_id;
        $table->sprav_uch_god_id = $sprav_uch_god_id;
        $table->archive = 0;
        $table->up_profiles_id = $up_profiles_id;

        $table->created_at = time();
        $table->created_by = Yii::$app->user->getId();
        $table->updated_at = time();
        $table->updated_by = Yii::$app->user->getId();
        $table->active = 1;
        $table->lock = 1;

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }

        $this->main_plan_id = $table->id;
    }

    private function parse_podpisants_table() {
        $path_dl = eval(Paths::$path_to_ДолжностныеЛица);
        $path_dl_plans = eval(Paths::$path_to_ДолжЛица_Планы);

        $do_id = '';
        foreach ($path_dl_plans as $dl) {
                if (((string) $dl['Утвердил']) == 'true') {
                    $do_id = (string) $dl['КодДолжЛица'];
                    break;
                }
            }
        $dl_f = ''; $dl_n = ''; $dl_o = '';
        foreach ($path_dl as $dl) {
            if (((string) $dl['Код']) == $do_id) {
                $fio = explode(' ', (string) $dl['ФИО']);
                $dl_f = $fio[0];
                $dl_n = explode('.', $fio[1])[0];
                $dl_o = explode('.', $fio[1])[1];
                break;
            }
        }
        $staff_id = (Staff::findOne(['f' => $dl_f, 'i' => $dl_n, 'o' => $dl_o]))->id;

        $table = new Podpisants();

        $table->updated_at = time();
        $table->updated_by = Yii::$app->user->getId();
        $table->staff_id = $staff_id;
        $table->main_plan_id = $this->main_plan_id;

        $table->created_at = time();
        $table->created_by = Yii::$app->user->getId();
        $table->updated_at = time();
        $table->updated_by = Yii::$app->user->getId();
        $table->active = 1;
        $table->lock = 1;

        if (!$table->save()) {
            $this->not_parse_errors = false;
        }
    }

    private function parse_comp_ps_has_comp_table() {
        $path = eval(Paths::$path_to_псФункции);
        if ($path['Функция'] == null) { return; }

        $path_pk_ps_functions = eval(Paths::$path_to_ПланыКомпетенцииПрофСтандарты);
        $path_ps_functions = eval(Paths::$path_to_псФункции);
        $path_pk = eval(Paths::$path_to_ПланыКомпетенции);

        foreach ($path_pk_ps_functions as $pk_ps_function) {
            $table = new CompPsHasComp();

            $code_pk = (string) $pk_ps_function['КодКомпетенции'];
            $code_ps_function = (string) $pk_ps_function['КодТФ'];
            if ($code_ps_function == '') { continue; }

            $name = '';
            foreach ($path_pk as $pk) {
                if (((string) $pk['Код']) == $code_pk) {
                    $name = (string) $pk['Наименование'];
                    break;
                }
            }
            $comp_id = (Comp::findOne(['soderzhanie' => $name]))->id;

            $func_name = '';
            foreach ($path_ps_functions as $function) {
                if (((string) $function['Код']) == $code_ps_function) {
                    $func_name = (string) $function['Функция'];
                    break;
                }
            }
            $comp_ps_id = (CompPs::findOne(['name' => $func_name]))->id;

            $table->comp_ps_id = $comp_ps_id;
            $table->comp_id = $comp_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    private function parse_calendary_table() {
        $path_cells = eval(Paths::$path_to_ПланыГрафикиЯчейки);
        $path_plans = eval(Paths::$path_to_Планы);
        $path_oop = eval(Paths::$path_to_ООП);

        $year = (integer) $path_plans['ГодНачалаПодготовки'];
        $i = 0;
        foreach ($path_cells as $cell) {
            $i++;
            $table = new Calendary();

            $course = (integer) $cell['Курс'];

            $kurs_id = (Kurs::findOne(['number_kurs' => $course]))->id;

            //Определяем дату начала нового курса
            $start_date = new \DateTime(((string)($year+$course-1)). "-09-01");

            $period_number = (string) $cell['КодВидаДеятельности'];
            $type_periods_id = (TypePeriods::findOne(['name' => $period_number]))->id;

            $date = new \DateTime($start_date->format('c'));
            $days = ((integer) $cell['НомерНедели']) * 7;

            if (((string) $cell['КоличествоЧастейВНеделе'] == '6')) {
                $date->add(new \DateInterval('P' . $days . 'D'));
                $begin = $date->format('Y-m-d');
                $date->add(new \DateInterval('P6D'));
                $end = $date->format('Y-m-d');
            } else {
                $date->add(new \DateInterval('P' . $days . 'D'));
                $begin = $date->format('Y-m-d');
                $days = ((integer) $cell['НомерЧастиНедели']) - 1;
                $date->add(new \DateInterval('P' . $days . 'D'));

                $end = $date->format('Y-m-d');
            }

            $table->begin = $begin;
            $table->end = $end;
            $table->kurs_id = $kurs_id;
            $table->type_periods_id = $type_periods_id;
            $table->main_plan_id = $this->main_plan_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
        $i;
    }

    private function parse_session_table() {
        $path_plans = eval(Paths::$path_to_Планы);
        $path_sessions = eval(Paths::$path_to_Заезды);//чтоб узнать название сессии, а впоследствии его айди
        $path_strocks_plans = eval(Paths::$path_to_ПланыСтроки);//Чтобы найти индекс дисциплины и потом получить его айди
        $path_selected_types_work = eval(Paths::$path_to_ВыбранныеВидыРабот);
        $path_sprav_type_works = eval(Paths::$path_to_СправочникВидыРабот);//чтобы узнать по коду название работы, а потом по названию айди работы

        //
        $path_hours = eval(Paths::$path_to_ПланыНовыеЧасы);

        //$data = (string) $path_plans['ЧислоСессий'];//Для чего?

        $batch = array();
        $batchField = array(
            'value',
            'type_session_id',
            'type_work_id',
            'disciplins_id',
            'kurs_id',
            'main_plan_id',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'deleted_by',
            'active',
            'lock',
        );

        foreach ($path_hours as $ph) {
            $value = (string) $ph['Количество'];//количество часов в поле
            
            //в каком семестре(сессии) это поле
            if((string) $ph['Семестр'] == 0){
                //Находим айди сессии
                $nameSession = '';//хранит название сессии, чтоб найти потом айди
                foreach($path_sessions as $ps){
                    if((string) $ps['Сессия'] == (string) $ph['Сессия']){
                        $nameSession = (string) $ps['Название'];
                        break;
                    }
                }
                //Получаем айди по названию:
                $type_session_id = (TypeSession::findOne(['name' => $nameSession]))->id;
            }else{
                //Находим айди семестра
                $nameSemestr = "Семестр ".((string) $ph['Семестр']);//хранит название семестра, чтоб найти потом айди

                //Получаем айди по названию:
                $type_session_id = (TypeSession::findOne(['name' => $nameSemestr]))->id;
            }
            
            //тип работы
            $nameWork = 0;//хранит название работы, чтобы потом найти айди работы
            foreach($path_sprav_type_works as $pstw){
                //Ищем Название работы
                if((string) $pstw['Код'] == (string) $ph['КодВидаРаботы']){
                    $nameWork = (string) $pstw['Название'];
                    break;
                }
            }
            //Получаем айди по имени типа работы
            $type_work_id = (TypeWork::findOne(['name' => $nameWork]))->id;
            
            //дисциплины
            $nameDisc = 0;//индекс дисциплины, чтобы найти айди
            foreach($path_strocks_plans as $psp){
                if((string)  $psp['Код'] == (string) $ph['КодОбъекта']){
                    $nameDisc = (string) $psp['ДисциплинаКод'];
                    break;
                }
            }
            //Get ID
            $disciplins_id = (Disciplins::findOne(['index' => $nameDisc, 'main_plan_id' => $this->main_plan_id]))->id;
            
            //получаем айди
            $kurs_id = (Kurs::findOne(['number_kurs' => (string) $ph['Курс']]))->id;

            $temp = array();

            $temp[] = $value;
            $temp[] = $type_session_id;
            $temp[] = $type_work_id;
            $temp[] = $disciplins_id;
            $temp[] = $kurs_id;
            $temp[] = $this->main_plan_id;
            $temp[] = time(); //created_at
            $temp[] = Yii::$app->user->getId(); //created_by
            $temp[] = time(); //updated_at
            $temp[] = Yii::$app->user->getId(); //updated_by
            $temp[] = 0; //deleted_by
            $temp[] = 1; //active
            $temp[] = 1; //lock
            $batch[] = $temp;
        }
        if (!Yii::$app->db->createCommand()->batchInsert('session',$batchField,$batch)->execute()) {
            Yii::info('Дисциплины не добавлены.');
            $this->not_parse_errors = false;
        }
    }

    private function parse_dc_table(){
        $path_comp = eval(Paths::$path_to_ПланыКомпетенции);
        $path_disciplins = eval(Paths::$path_to_ПланыСтроки);
        $path_to_dc = eval(Paths::$path_to_ПланыКомпетенцииДисциплины);

        foreach ($path_to_dc as $dc) {
            $disciplins_code = (int) $dc['КодСтроки'];
            $comp_code = (int) $dc['КодКомпетенции'];

            $comp_name = '';
            foreach ($path_comp as $comp) {
                if ($comp_code == ((int) $comp['Код'])) {
                    $comp_name = (string) $comp['ШифрКомпетенции'];
                }
            }
            $comp_id = (Comp::findOne(['index' => $comp_name, 'main_plan_id'=>$this->main_plan_id]))->id;

            $disciplin_name = '';
            foreach ($path_disciplins as $disciplin) {
                if ($disciplins_code == ((int) $disciplin['Код'])) {
                    $disciplin_name = (string) $disciplin['ДисциплинаКод'];
                }
            }
            $disciplin_id = (Disciplins::findOne(['index' => $disciplin_name, 'main_plan_id' => $this->main_plan_id]))->id;

            $table = new Dc();

            $table->comp_id = $comp_id;
            $table->disciplins_id = $disciplin_id;

            $table->created_at = time();
            $table->created_by = Yii::$app->user->getId();
            $table->updated_at = time();
            $table->updated_by = Yii::$app->user->getId();
            $table->active = 1;
            $table->lock = 1;

            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
        }
    }

    public function parse_up_profiles()
    {
        $path_up_profiles = eval(Paths::$path_to_ООП_ООП);
        foreach ($path_up_profiles as $up) {
            $prof = (string)$up['Название'];
            if (UpProfiles::find()->andWhere(['name' => $prof])->count() == 1) {
                $table = UpProfiles::findOne(['name' => $prof]);
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
            } else {
                $table = new UpProfiles();
                $table->name = $prof;

                $table->created_at = time();
                $table->created_by = Yii::$app->user->getId();
                $table->updated_at = time();
                $table->updated_by = Yii::$app->user->getId();
                $table->active = 1;
                $table->lock = 0;
            }
            if (!$table->save()) {
                $this->not_parse_errors = false;
            }
            else{
                $this->dataParse['up_profiles_id']=$table->id;
            }
        }
    }

    private function save_all($transaction) {
        /* Если ошибок при записи данных не было,
         * то изменения сохраняются, иначе откатываются
         */
        if ($this->not_parse_errors) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollback();
            return false;
        }
    }

    public function parse() {
        /* Функция получает файл пользователя и парсит его,
         * вызывая функции парсинга данных для определённых
         * таблиц
         */

        $data = file_get_contents($this->file->tempName);

        $this->xml = new \SimpleXMLElement($data);

        $transaction = Yii::$app->db->beginTransaction();

        $this->parse_sroc_education_table(); // 1
        $this->parse_staff_table(); // 2
        $this->parse_fgos_table(); // 3
        $this->parse_kvalification_table(); // 4
        $this->parse_fo_table(); // 5
        $this->parse_prof_standart_table(); // 6
        $this->parse_comp_ps_table(); // 7
        $this->parse_type_task_pd_table(); // 8
        $this->parse_np_table(); // 9
        $this->parse_institut_table(); // 10
        $this->parse_sprav_facultet_table(); // 11
        $this->parse_sprav_kafedra_table(); // 12
        $this->parse_sprav_uch_god_table(); // 13
        $this->parse_up_profiles(); //31

        $this->parse_main_plan_table(); // 14 Должна быть всегда включена для корректной работы идущих далее функций

        $this->parse_podpisants_table(); // 15
        $this->parse_comp_table(); // 16
        $this->parse_type_periods_table(); // 17
        $this->parse_kurs_table(); // 21
        $this->parse_calendary_table(); // 18
        $this->parse_comp_ps_has_comp_table(); // 19
        $this->parse_sprav_dis_table(); // 20
        $this->parse_disciplins_table(); // 23
        $this->parse_type_form_table(); // 24
        $this->parse_form_table(); // 25
        $this->parse_kaf_has_comp_table(); // 26
        $this->parse_type_work_table(); // 27
        $this->parse_type_session_table(); // 28
        $this->parse_session_table(); // 29
        $this->parse_dc_table(); // 30




        // Если УП дублируется, то добавляем в таблицы зависимости с новым УП
        if ($this->is_double) {
            $ArchiveMainPlan = new ArchiveMainPlan();

            // если были ошибки, то изменения не применяются
            if (!$ArchiveMainPlan->updateRelations($this->main_plan_id, $this->archive_main_plan_id)) {
                $this->not_parse_errors = false;
            }
        }

        return $this->save_all($transaction); // сохраняем все изменения в БД
    }
}