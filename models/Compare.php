<?php


namespace backend\modules\Parser\models;


use backend\models\tables\Comp;
use backend\models\tables\Dc;
use common\models\SpravDis;
use common\models\Disciplins;

class Compare extends \yii\base\Model
{
    public function compare($id_main_plan_1, $id_main_plan_2)
    {
        $data = [
            'index_1' => [],
            'name_1' => [],
            'comp_1' => [],
            'index_2' => [],
            'name_2' => [],
            'comp_2' => [],
            'is_equal' => [],
        ];


        // добавляются данные из первого плана
        $all_dis_array = Disciplins::find()->andWhere(['main_plan_id' => $id_main_plan_1])->orderBy([
            'index' => SORT_ASC
        ])->all();
        foreach ($all_dis_array as $dis) {
            // добалвяется индекс
            array_push($data['index_1'], $dis->index);

            // добавляется название
            $dis_name = (SpravDis::findOne(['id' => $dis->sprav_dis_id]))->name;
            array_push($data['name_1'], $dis_name);

            // добавление компетенций
            $comp_array = [];
            $all_dc = Dc::find()->andWhere(['disciplins_id' => $dis->id])->all();
            foreach ($all_dc as $dc) {
                $comp = (Comp::findOne(['id' => $dc->comp_id]))->index;
                array_push($comp_array, $comp);
            }
            $unique_comp = array_unique($comp_array);
            array_push($data['comp_1'], $unique_comp);
        }

        // добавляются данные из второго плана
        $all_dis_array = Disciplins::find()->andWhere(['main_plan_id' => $id_main_plan_2])->orderBy([
            'index' => SORT_ASC
        ])->all();
        foreach ($all_dis_array as $dis) {
            // добалвяется индекс
            array_push($data['index_2'], $dis->index);

            // добавляется название
            $dis_name = (SpravDis::findOne(['id' => $dis->sprav_dis_id]))->name;
            array_push($data['name_2'], $dis_name);

            // добавление компетенций
            $comp_array = [];
            $all_dc = Dc::find()->andWhere(['disciplins_id' => $dis->id])->all();
            foreach ($all_dc as $dc) {
                $comp = (Comp::findOne(['id' => $dc->comp_id]))->index;
                array_push($comp_array, $comp);
            }
            $unique_comp = array_unique($comp_array);
            array_push($data['comp_2'], $unique_comp);
        }

        // сравнение двух планов
        $is_qual_array = [];
        $arr_qual = [
            'name' => '',
            'index' => '',
            'comp' => '',
        ];
        $plan_1_row_counter = 0;
        foreach ($data['name_1'] as $name_1) {
            // берётся дисциплина из 1-го плана и ищется соответсвующая ей дисциплина из 2-го
            $plan_2_row_counter = 0;
            foreach ($data['name_2'] as $name_2) {
                // ищется дисциплина из 1-го плана среди всех дисциплин 2-го
                if ($name_1 == $name_2) {
                    // array_push($is_qual_array['name'], true);//ячейка по имени совпадает
                    $arr_qual['name'] = true;
                    break;
                }
                // array_push($is_qual_array['name'], false);//ячейка по имени не совпадает
                $arr_qual['name'] = false;
                $plan_2_row_counter++;
            }


            //Такой строки во втором плане просто нет, незачем и код выполнять
            if (!array_key_exists($plan_2_row_counter,$data['index_2'])){
                // array_push($is_qual_array['name'], false);
                // array_push($is_qual_array['index'], false);
                // array_push($is_qual_array['comp'], false);
                $arr_qual['name'] = false;
                $arr_qual['index'] = false;
                $arr_qual['comp'] = false;
                array_push($is_qual_array, $arr_qual);//
                $plan_1_row_counter++;
                continue;
            }

            $qual = true; // равны ли строки
            // array_push($is_qual_array['index'], $qual);
            $arr_qual['index'] = $qual;
            // сравнение по индексу
            if ($data['index_1'][$plan_1_row_counter] != $data['index_2'][$plan_2_row_counter]) {
                $qual = false;
                // array_push($is_qual_array['index'], $qual);
                $arr_qual['index'] = $qual;
                //continue;
            }
            // сравнение comp
            $comp_1 = $data['comp_1'][$plan_1_row_counter];
            $comp_2 = $data['comp_2'][$plan_2_row_counter];
            $comp_1_length = count($comp_1);
            $comp_2_length = count($comp_2);

            $comp_qual = [];//хранит совпадения по компетенциям в строчке
            $cp_id = 0;

            if ($comp_1_length === $comp_2_length){
                foreach($comp_1 as $cp1){//перебираем компетенции первого плана
                    $comp_qual[$cp_id] = false;
                    foreach($comp_2 as $cp2){//компетенции второго плана
                        if($cp1 == $cp2){//если они совпадают, то отмечаем это в массиве
                            $comp_qual[$cp_id] = true;
                            break;
                        }
                    }
                    // if($comp_qual[$cp_id] != true){//если не был отмечен, то отмечаем как false
                    //     $comp_qual[$cp_id] = false;
                    // }
                    $cp_id++;//обновляем счётчик для массива
                }
                //ставим qual в false, если есть отличие, хотя бы в одной компетенции
                $qual = true;
                for($i = 0; $i < $cp_id; $i++){
                    if($comp_qual[$i] == false){
                        $qual = false;
                        break;
                    }
                }
//                 foreach ($comp_1 as $basicValue){
//                     $find = false;
//                     foreach ($comp_2 as $compareValue){
//                         if ($basicValue == $compareValue){
//                             $find = true;
//                         }
//                     }
//                     if ($find === true){
//                         continue;
//                     }
//                 }
//                 if ($find == false){
//                     $qual = false;
//                 }
//                 /*for ($i = 0; $i < $comp_1_length; $i++) {
//                     if ($comp_1[$i] != $comp_2[$i]) {
//                         $qual = false;
//                     }
//                 }*/
            }
            else {
                $qual = false;
            }
            // array_push($is_qual_array['comp'], $qual);
            $arr_qual['comp'] = $qual;
            array_push($is_qual_array, $arr_qual);
            $plan_1_row_counter++;
        }

        foreach ($is_qual_array as $qual) {
            array_push($data['is_equal'], $qual);
        }

        return $data;
    }

    /*
    private function compareFields(array $data, string $field, int $idBasic){
        //Запускаем поиск по всем параметрам
        $fieldKey = $field . "_1";
        if (!(array_key_exists($fieldKey,$data) && array_key_exists($idBasic,$data[$fieldKey]))){
            return false;
        }

        $tempRow = $data[$fieldKey][$idBasic];

        //Начинаем обход второго массива в поисках необходимого элемента
    }*/

    public function compare_comp($comp_data) {
        /* Функция для сравнения компетенций */
        $result_array = [];

        // пробегаем по всем компетенциям 1-го плана
        for ($row_2 = 0; $row_2 < count($comp_data['index_2']); $row_2++) {
            // берём компетенцию из 1-го плана и ищем совпадающию из 2-го
            $comp_row_1 = 0;
            foreach ($comp_data['index_1'] as $index_1) {
                if ($index_1 == $comp_data['index_2'][$row_2]) {
                    break;
                }
                $comp_row_1++;
            }

            //если такой строки во втором плане просто нет, незачем и код выполнять
            if (!array_key_exists($comp_row_1, $comp_data['index_1'])){
                array_push($result_array, false);
                continue;
            }

            $is_equal = true; // равны ли строки

            // сравниваем индексы и названия
            if ($comp_data['index_2'][$row_2] != $comp_data['index_1'][$comp_row_1] or
                $comp_data['name_2'][$row_2] != $comp_data['name_1'][$comp_row_1]) {
                $is_equal = false;
            }

            array_push($result_array, $is_equal);
        }

        return $result_array;
    }

    public function get_comp_data($id_main_plan_1, $id_main_plan_2) {
        /* Функция для формирования массива с данными
         * по компетенциям из двух планов и результатом из сравнения
         */
        $comp_data = [
            'index_1' => [],
            'name_1' => [],
            'index_2' => [],
            'name_2' => [],
            'is_equal' => [],
        ];

        // получение всех компетенций для 1-го плана
        $all_comps = Comp::find()->andWhere(['main_plan_id' => $id_main_plan_1])->orderBy([
            'index' => SORT_ASC,
        ])->all();
        foreach ($all_comps as $comp) {
            // заполняем массив с данными
            array_push($comp_data['index_1'], $comp->index);
            array_push($comp_data['name_1'], $comp->soderzhanie);
        }

        // получение всех компетенций для 2-го плана
        $all_comps = Comp::find()->andWhere(['main_plan_id' => $id_main_plan_2])->orderBy([
            'index' => SORT_ASC,
        ])->all();
        foreach ($all_comps as $comp) {
            // заполняем массив с данными
            array_push($comp_data['index_2'], $comp->index);
            array_push($comp_data['name_2'], $comp->soderzhanie);
        }

        // сравниваем компетенции и добавляем результат в массив с данными
        $result = $this->compare_comp($comp_data);
        foreach ($result as $res) {
            array_push($comp_data['is_equal'], $res);
        }

        return $comp_data;
    }
}
