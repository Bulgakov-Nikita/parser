<?php

namespace backend\modules\Parser\models\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\backend\models\tables\MainPlan]].
 *
 * @see \backend\models\tables\MainPlan
 */
class MainPlanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\tables\MainPlan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\tables\MainPlan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
