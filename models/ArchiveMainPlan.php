<?php

namespace backend\modules\Parser\models;

use backend\models\tables\RoomHasDisciplins;
use common\models\Dp;
use backend\models\tables\Disciplins;
use Yii;

class ArchiveMainPlan {
    private $new_main_plan; // id нового УП
    private $old_main_plan; // id старого УП
    private $not_errors = true; // были ли ошибки при добавлении данных

    private function UpdateDpRelations() {
        $all_dp = Dp::find()->all(); // все связи преподавателей и дисциплин

        // перебираем все связи
        foreach ($all_dp as $dp) {
            $disciplin = Disciplins::findOne(['id' => $dp->disciplins_id]); // находим дисциплину

            // если у дисциплины старый main_plan_id, то ищем такую же, но с id нового mp и добавляем запись в dp
            if ($disciplin->main_plan_id == $this->old_main_plan) {
                // ищем дисциплину из старого УП в новом
                $new_disciplin = Disciplins::findOne([
                    'index' => $disciplin->index,
                    'sprav_dis_id' => $disciplin->sprav_dis_id,
                    'sprav_kafedra_id' => $disciplin->sprav_kafedra_id,
                    'main_plan_id' =>  $this->new_main_plan,
                ]);

                $dp->active = 0; // делаем старую связь неактивной
                if (!$dp->save()) {
                    $this->not_errors = false;
                }

                // если такая дисциплина существует, добавляем запись в dp, иначе пропускаем
                if ($new_disciplin != null) {
                    $table = new Dp();

                    $table->fl_id = $dp->fl_id;
                    $table->disciplins_id = $new_disciplin->id;

                    $table->created_at = time();
                    $table->created_by = Yii::$app->user->getId();
                    $table->updated_at = time();
                    $table->updated_by = Yii::$app->user->getId();
                    $table->active = 1;
                    $table->lock = 1;

                    if (!$table->save()) {
                        $this->not_errors = false;
                    }
                } else {
                    continue;
                }
            }
        }
    }

    private function updateRoomHasDisciplinsRelations() {
        $all_rhd = RoomHasDisciplins::find()->all(); // все связи аудиторий и дисциплин

        // перебираем все связи
        foreach ($all_rhd as $rhd) {
            $disciplin = Disciplins::findOne(['id' => $rhd->disciplins_id]); // находим дисциплину

            // если у дисциплины старый main_plan_id, то ищем такую же, но с id нового mp и добавляем запись в rhd
            if ($disciplin->main_plan_id == $this->old_main_plan) {
                // ищем дисциплину из старого УП в новом
                $new_disciplin = Disciplins::findOne([
                    'index' => $disciplin->index,
                    'sprav_dis_id' => $disciplin->sprav_dis_id,
                    'sprav_kafedra_id' => $disciplin->sprav_kafedra_id,
                    'main_plan_id' =>  $this->new_main_plan,
                ]);

                $rhd->active = 0; // делаем старую связь неактивной
                if (!$rhd->save()) {
                    $this->not_errors = false;
                }

                // если такая вдисциплина существует, добавляем запись в dp, иначе пропускаем
                if ($new_disciplin != null) {
                    $table = new RoomHasDisciplins();

                    $table->room_id = $rhd->room_id;
                    $table->disciplins_id = $new_disciplin->id;

                    $table->created_at = time();
                    $table->created_by = Yii::$app->user->getId();
                    $table->updated_at = time();
                    $table->updated_by = Yii::$app->user->getId();
                    $table->active = 1;
                    $table->lock = 1;

                    if (!$table->save()) {
                        $this->not_errors = false;
                    }
                } else {
                    continue;
                }
            }
        }
    }

    public function updateRelations($main_plan_id, $archive_main_plan_id) {
        // главная функция, которая вызывает вспомогательные для обновления данных в таблицах

        $this->new_main_plan = $main_plan_id;
        $this->old_main_plan = $archive_main_plan_id;

        // вызываем функции обновления связей для таблиц
        $this->updateDpRelations(); // 1
        $this->updateRoomHasDisciplinsRelations(); // 2

        return $this->not_errors;
    }
}