<?php

namespace backend\modules\Parser\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\tables\MainPlan;

/**
 * backend\modules\Parser\models\search\MainPlanSearch represents the model behind the search form about `backend\modules\Parser\models\tables\MainPlan`.
 */
 class MainPlanSearch extends MainPlan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date_sp', 'fgos_id', 'sprav_kafedra_id', 'kvalification_id', 'np_id', 'fo_id', 'staff_id', 'sroc_education_id', 'sprav_uch_god_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'lock'], 'integer'],
            [['date_ut', 'name_or', 'name_ministry', 'n_protocol', 'date_protocol', 'active', 'archive'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainPlan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_sp' => $this->date_sp,
            'date_ut' => $this->date_ut,
            'date_protocol' => $this->date_protocol,
            'fgos_id' => $this->fgos_id,
            'sprav_kafedra_id' => $this->sprav_kafedra_id,
            'kvalification_id' => $this->kvalification_id,
            'np_id' => $this->np_id,
            'fo_id' => $this->fo_id,
            'staff_id' => $this->staff_id,
            'sroc_education_id' => $this->sroc_education_id,
            'sprav_uch_god_id' => $this->sprav_uch_god_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'name_or', $this->name_or])
            ->andFilterWhere(['like', 'name_ministry', $this->name_ministry])
            ->andFilterWhere(['like', 'n_protocol', $this->n_protocol])
            ->andFilterWhere(['like', 'active', $this->active])
            ->andFilterWhere(['like', 'archive', $this->archive]);

        return $dataProvider;
    }
}
