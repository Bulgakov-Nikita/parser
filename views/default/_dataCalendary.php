<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->calendaries,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'begin',
        'end',
        [
                'attribute' => 'typePeriods.name',
                'label' => 'Type Periods'
            ],
        [
            'attribute' => 'kurs_id',
            'label' => 'Курс',
            'value' => function($model){
                $res = $model->getKurs()->one();

                return is_object($res) ? $res->number_kurs : NULL;
            }
        ],
        'active',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'calendary'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
