<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\Parser\models\tables\MainPlan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Main Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-plan-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Main Plan'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=
            Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            'date_sp',
            'date_ut',
            'name_or:ntext',
            'name_ministry:ntext',
            'n_protocol',
            'date_protocol',
            [
                'attribute' => 'fgos.id',
                'label' => 'Fgos',
            ],
            [
                'attribute' => 'spravKafedra.name',
                'label' => 'Sprav Kafedra',
            ],
            [
                'attribute' => 'kvalification.name',
                'label' => 'Kvalification',
            ],
            [
                'attribute' => 'np.name',
                'label' => 'Np',
            ],
            [
                'attribute' => 'fo.name',
                'label' => 'Fo',
            ],
            [
                'attribute' => 'staff.id',
                'label' => 'Staff',
            ],
            [
                'attribute' => 'srocEducation.name',
                'label' => 'Sroc Education',
            ],
            [
                'attribute' => 'spravUchGod.name',
                'label' => 'Sprav Uch God',
            ],
            'active',
            'archive',
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>

    <div class="row">
        <?php
        if($providerCalendary->totalCount){
            $gridColumnCalendary = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                'begin',
                'end',
                [
                    'attribute' => 'typePeriods.name',
                    'label' => 'Type Periods'
                ],
                [
                    'attribute' => 'kurs.id',
                    'label' => 'Kurs'
                ],
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerCalendary,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-calendary']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Calendary'),
                ],
                'columns' => $gridColumnCalendary
            ]);
        }
        ?>

    </div>

    <div class="row">
        <?php
        if($providerComp->totalCount){
            $gridColumnComp = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                'index',
                'soderzhanie:ntext',
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerComp,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comp']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Comp'),
                ],
                'columns' => $gridColumnComp
            ]);
        }
        ?>

    </div>

    <div class="row">
        <?php
        if($providerDisciplins->totalCount){
            $gridColumnDisciplins = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                'index',
                [
                    'attribute' => 'parent.id',
                    'label' => 'Parent'
                ],
                [
                    'attribute' => 'spravDis.name',
                    'label' => 'Sprav Dis'
                ],
                [
                    'attribute' => 'spravKafedra.name',
                    'label' => 'Sprav Kafedra'
                ],
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerDisciplins,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-disciplins']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Disciplins'),
                ],
                'columns' => $gridColumnDisciplins
            ]);
        }
        ?>

    </div>

    <div class="row">
        <?php
        if($providerGmp->totalCount){
            $gridColumnGmp = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'groups.name',
                    'label' => 'Groups'
                ],
                'date_zakrep',
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerGmp,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gmp']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Gmp'),
                ],
                'columns' => $gridColumnGmp
            ]);
        }
        ?>

    </div>
    <div class="row">
        <h4>Fgos<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnFgos = [
        ['attribute' => 'id', 'visible' => false],
        'number',
        'date',
        'path_file',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->fgos,
        'attributes' => $gridColumnFgos    ]);
    ?>
    <div class="row">
        <h4>Fo<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnFo = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->fo,
        'attributes' => $gridColumnFo    ]);
    ?>
    <div class="row">
        <h4>SpravKafedra<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnSpravKafedra = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'sprav_facultet_id',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->spravKafedra,
        'attributes' => $gridColumnSpravKafedra    ]);
    ?>
    <div class="row">
        <h4>Kvalification<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnKvalification = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->kvalification,
        'attributes' => $gridColumnKvalification    ]);
    ?>
    <div class="row">
        <h4>Np<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnNp = [
        ['attribute' => 'id', 'visible' => false],
        'code',
        'name',
        'type_task_pd_id',
        'comp_ps_id',
        'active',
        'up_profiles_id',
    ];
    echo DetailView::widget([
        'model' => $model->np,
        'attributes' => $gridColumnNp    ]);
    ?>
    <div class="row">
        <h4>SpravUchGod<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnSpravUchGod = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'year_begin',
        'year_end',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->spravUchGod,
        'attributes' => $gridColumnSpravUchGod    ]);
    ?>
    <div class="row">
        <h4>SrocEducation<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnSrocEducation = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->srocEducation,
        'attributes' => $gridColumnSrocEducation    ]);
    ?>
    <div class="row">
        <h4>Staff<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnStaff = [
        ['attribute' => 'id', 'visible' => false],
        'f',
        'i',
        'o',
        'post',
        'user_id',
        'active',
    ];
    echo DetailView::widget([
        'model' => $model->staff,
        'attributes' => $gridColumnStaff    ]);
    ?>

    <div class="row">
        <?php
        if($providerNid->totalCount){
            $gridColumnNid = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                'code',
                'perechen:ntext',
                'result:ntext',
                'svedenia:ntext',
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerNid,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-nid']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Nid'),
                ],
                'columns' => $gridColumnNid
            ]);
        }
        ?>

    </div>

    <div class="row">
        <?php
        if($providerPodpisants->totalCount){
            $gridColumnPodpisants = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'staff.id',
                    'label' => 'Staff'
                ],
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerPodpisants,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-podpisants']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Podpisants'),
                ],
                'columns' => $gridColumnPodpisants
            ]);
        }
        ?>

    </div>

    <div class="row">
        <?php
        if($providerSession->totalCount){
            $gridColumnSession = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                'value',
                [
                    'attribute' => 'typeSession.name',
                    'label' => 'Type Session'
                ],
                [
                    'attribute' => 'typeWork.name',
                    'label' => 'Type Work'
                ],
                [
                    'attribute' => 'disciplins.id',
                    'label' => 'Disciplins'
                ],
                [
                    'attribute' => 'kurs.id',
                    'label' => 'Kurs'
                ],
                'active',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerSession,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-session']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Session'),
                ],
                'columns' => $gridColumnSession
            ]);
        }
        ?>

    </div>
</div>
