<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->disciplins,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'index',
        [
                'attribute' => 'parent.id',
                'label' => 'Parent'
            ],
        [
                'attribute' => 'spravDis.name',
                'label' => 'Sprav Dis'
            ],
        [
                'attribute' => 'spravKafedra.name',
                'label' => 'Sprav Kafedra'
            ],
        'active',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'disciplins'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
