<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\Parser\models\search\MainPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Импортирование учебного плана';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-plan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->renderAjax('import_file',[
            'model'=>$model,
        ]); ?>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute'=>'name',
            'label' => 'Название УП',
            'value' => function($model){
                return $model->strNamePlan();
            }
        ],
        'date_sp',
//        'date_ut',
//        'name_or:ntext',
//        'name_ministry:ntext',
//        'n_protocol',
//        'date_protocol',
        /*[
                'attribute' => 'fgos_id',
                'label' => 'ФГОС',
                'value' => function($model){                   
                    return $model->fgos->id;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\Parser\models\tables\Fgos::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Fgos', 'id' => 'grid-main-plan-search-fgos_id']
            ],
        [
                'attribute' => 'sprav_kafedra_id',
                'label' => 'Кафедра',
                'value' => function($model){                   
                    return $model->spravKafedra->name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\Parser\models\tables\SpravKafedra::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Sprav kafedra', 'id' => 'grid-main-plan-search-sprav_kafedra_id']
            ],
        [
                'attribute' => 'kvalification_id',
                'label' => 'Квалификация',
                'value' => function($model){
                    if ($model->kvalification)
                    {return $model->kvalification->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\Parser\models\tables\Kvalification::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Квалификация', 'id' => 'grid-main-plan-search-kvalification_id']
            ],*/
        [
                'attribute' => 'np_id',
                'label' => 'Направление подготовки',
                'value' => function($model){                   
                    return $model->np->name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\models\tables\Np::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Фильтр', 'id' => 'grid-main-plan-search-np_id']
            ],
        [
            'attribute' => 'up_profiles_id',
            'label' => 'Профиль',
            'value' => function ($model){
                $res = $model->upProfiles;
                return $res->name ?: '--';
            }
        ],
        [
                'attribute' => 'fo_id',
                'label' => 'Форма обучения',
                'value' => function($model){                   
                    return $model->fo->name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\models\tables\Fo::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Фильтр', 'id' => 'grid-main-plan-search-fo_id']
            ],
        /*[
                'attribute' => 'staff_id',
                'label' => 'Staff',
                'value' => function($model){                   
                    return $model->staff->id;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\Parser\models\tables\Staff::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Staff', 'id' => 'grid-main-plan-search-staff_id']
            ],*/
        [
                'attribute' => 'sroc_education_id',
                'label' => 'Срок обучения',
                'value' => function($model){                   
                    return $model->srocEducation->name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\models\tables\SrocEducation::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Фильтр', 'id' => 'grid-main-plan-search-sroc_education_id']
            ],
        /*[
                'attribute' => 'sprav_uch_god_id',
                'label' => 'Учебный год',
                'value' => function($model){                   
                    return $model->spravUchGod->name;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\modules\Parser\models\tables\SpravUchGod::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Учебный год', 'id' => 'grid-main-plan-search-sprav_uch_god_id']
            ],*/
//        'active',
        'archive',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Просмотр',
            'template' => '{details}',
            'buttons' => [
                'details' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'lead-view'),
                    ]);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-main-plan']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
