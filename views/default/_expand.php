<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('MainPlan'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Calendary'),
        'content' => $this->render('_dataCalendary', [
            'model' => $model,
            'row' => $model->calendaries,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Comp'),
        'content' => $this->render('_dataComp', [
            'model' => $model,
            'row' => $model->comps,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Disciplins'),
        'content' => $this->render('_dataDisciplins', [
            'model' => $model,
            'row' => $model->disciplins,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Gmp'),
        'content' => $this->render('_dataGmp', [
            'model' => $model,
            'row' => $model->gmps,
        ]),
    ],
                                            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Nid'),
        'content' => $this->render('_dataNid', [
            'model' => $model,
            'row' => $model->ns,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Podpisants'),
        'content' => $this->render('_dataPodpisants', [
            'model' => $model,
            'row' => $model->podpisants,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Session'),
        'content' => $this->render('_dataSession', [
            'model' => $model,
            'row' => $model->sessions,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
