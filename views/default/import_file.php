<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\Parser\models\Parser */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Импортирование учебного плана';
?>

<p id="error_message_save" class="text-danger" style="display:none;font-size: 12px;height: auto;font-family: 'Open Sans', sans-serif;"></p>
<p id="load_res" class="text-success box-success" style="display:none;font-size: 14px;float: right;"></p>

<style>
    .input-file {
        background: green;
    }
</style>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],
    'id'=>'import_file_form',
]) ?>

<?= $form->field($model, 'file')->fileInput([
    'id' => "uploaded-files",
    'multiple' => false,
    'class' => 'btn btn-success',
    'label' => 'text',
])->label(false) ?>

<?=
Html::submitButton('Отправить учебный план', ['class' => 'btn btn-success',
    'id' => 'btnId',
    'onClick' => '      
                       var fi = document.getElementById(\'uploaded-files\');
                       
                       if (fi.files.length == 0) {
                            $(\'#error_message_save\').html("Выберите файлы").show();
                            return false;
                       }
                        else {  
                            var formElement = document.querySelector("#import_file_form");
                            var fd = new FormData(formElement);
                        
                            $.ajax({
                                url: \'/parser/default/load-plan/\',
                                type: "POST",
                                data :fd,
                                processData: false,
                                contentType: false,
                                cache: false,
                                beforeSend: function() {
                                $(\'#error_message_save\').hide();
                                    $(\'#load_res\').html(\'Загружается...<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\').show(); 
                                
                                },
                                complete: function(result) {
                                    console.log(result);
                                    response = result.responseJSON;
                                    $(\'#load_res\').hide();
                                    if (response.error == true) {
                                        $(\'#error_message_save\').html(\'<h5 style="color:red">\'+response.desc+\'</h5>\').show();
                                    } else {
                                    $(\'#error_message_save\').html(\'<h5 style="color:green">Загрузка успешно завершена</h5>\').show();
                                    }
                                   $.pjax.reload({
                                        container:\'#kv-pjax-container-main-plan\',
                                    })
                                },
                                success: function() {
                                   
                                    $(\'#error_message_save\').text("Загрузка завершена");
                                    $(\'#load_res\').hide();
                                    $.pjax.reload({
                                        container:\'#kv-pjax-container-main-plan\',
                                    })
                                },

                            });
                            
                        }
                        
                        return false; //запрет перезагрузки страницы
']) ?>

<?php ActiveForm::end() ?>


