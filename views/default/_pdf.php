<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\Parser\models\tables\MainPlan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Main Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-plan-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Main Plan'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'date_sp',
        'date_ut',
        'name_or:ntext',
        'name_ministry:ntext',
        'n_protocol',
        'date_protocol',
        [
                'attribute' => 'fgos.id',
                'label' => 'Fgos'
            ],
        [
                'attribute' => 'spravKafedra.name',
                'label' => 'Sprav Kafedra'
            ],
        [
                'attribute' => 'kvalification.name',
                'label' => 'Kvalification'
            ],
        [
                'attribute' => 'np.name',
                'label' => 'Np'
            ],
        [
                'attribute' => 'fo.name',
                'label' => 'Fo'
            ],
        [
                'attribute' => 'staff.id',
                'label' => 'Staff'
            ],
        [
                'attribute' => 'srocEducation.name',
                'label' => 'Sroc Education'
            ],
        [
                'attribute' => 'spravUchGod.name',
                'label' => 'Sprav Uch God'
            ],
        'active',
        'archive',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCalendary->totalCount){
    $gridColumnCalendary = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'begin',
        'end',
        [
                'attribute' => 'typePeriods.name',
                'label' => 'Type Periods'
            ],
        [
                'attribute' => 'kurs.id',
                'label' => 'Kurs'
            ],
                'active',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCalendary,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Calendary'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCalendary
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerComp->totalCount){
    $gridColumnComp = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'index',
        'soderzhanie:ntext',
                'active',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerComp,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Comp'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnComp
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerDisciplins->totalCount){
    $gridColumnDisciplins = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'index',
        [
                'attribute' => 'parent.id',
                'label' => 'Parent'
            ],
        [
                'attribute' => 'spravDis.name',
                'label' => 'Sprav Dis'
            ],
        [
                'attribute' => 'spravKafedra.name',
                'label' => 'Sprav Kafedra'
            ],
                'active',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDisciplins,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Disciplins'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnDisciplins
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerGmp->totalCount){
    $gridColumnGmp = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'groups.name',
                'label' => 'Groups'
            ],
                'date_zakrep',
        'active',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerGmp,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Gmp'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnGmp
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerNid->totalCount){
    $gridColumnNid = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'code',
                'perechen:ntext',
        'result:ntext',
        'svedenia:ntext',
        'active',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerNid,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Nid'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnNid
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPodpisants->totalCount){
    $gridColumnPodpisants = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'staff.id',
                'label' => 'Staff'
            ],
                'active',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPodpisants,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Podpisants'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPodpisants
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerSession->totalCount){
    $gridColumnSession = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'value',
        [
                'attribute' => 'typeSession.name',
                'label' => 'Type Session'
            ],
        [
                'attribute' => 'typeWork.name',
                'label' => 'Type Work'
            ],
        [
                'attribute' => 'disciplins.id',
                'label' => 'Disciplins'
            ],
        [
                'attribute' => 'kurs.id',
                'label' => 'Kurs'
            ],
        'active',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerSession,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Session'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnSession
    ]);
}
?>
    </div>
</div>
