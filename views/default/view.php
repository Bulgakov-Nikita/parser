<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\tables\SrocEducation;
use backend\models\tables\MainPlan;
use backend\models\tables\Np;
use backend\models\tables\Fo;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */

$this->title = 'Просмотр учебных планов';
?>

<?php
    echo '<table border=1px>';

    // выводим заголовки таблицы
    echo '<tr>
            <th>Название плана</th>
            <th>Год подготовки</th>
            <th>Срок обучения</th>
            <th>Направление подготовки</th>
            <th>Форма обучения</th>
         </tr>';

    // берём все не архивные УП
    $all_main_plans = MainPlan::find()->all();
    foreach ($all_main_plans as $main_plan) {
        echo '<tr>
            <td>' . $main_plan->strNamePlan() . '</td>
            <td>' . $main_plan->date_sp . '</td>
            <td>' . (SrocEducation::findOne(['id' => $main_plan->sroc_education_id]))->name . '</td>
            <td>' . (Np::findOne(['id' => $main_plan->np_id]))->name . '</td>
            <td>' . (Fo::findOne(['id' => $main_plan->fo_id]))->name . '</td>
         </tr>';
    }

    echo '</table>';
?>