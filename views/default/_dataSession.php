<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->sessions,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'value',
        [
                'attribute' => 'typeSession.name',
                'label' => 'Type Session'
            ],
        [
                'attribute' => 'typeWork.name',
                'label' => 'Type Work'
            ],
        [
                'attribute' => 'disciplins.id',
                'label' => 'Disciplins'
            ],
        [
                'attribute' => 'kurs.id',
                'label' => 'Kurs'
            ],
        'active',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'session'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
