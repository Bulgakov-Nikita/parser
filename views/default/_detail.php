<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\Parser\models\tables\MainPlan */

?>
<div class="main-plan-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'date_sp',
        'date_ut',
        'name_or:ntext',
        'name_ministry:ntext',
        'n_protocol',
        'date_protocol',
        [
            'attribute' => 'fgos.id',
            'label' => 'Fgos',
        ],
        [
            'attribute' => 'spravKafedra.name',
            'label' => 'Sprav Kafedra',
        ],
        [
            'attribute' => 'kvalification.name',
            'label' => 'Kvalification',
        ],
        [
            'attribute' => 'np.name',
            'label' => 'Np',
        ],
        [
            'attribute' => 'fo.name',
            'label' => 'Fo',
        ],
        [
            'attribute' => 'staff.id',
            'label' => 'Staff',
        ],
        [
            'attribute' => 'srocEducation.name',
            'label' => 'Sroc Education',
        ],
        [
            'attribute' => 'spravUchGod.name',
            'label' => 'Sprav Uch God',
        ],
        'active',
        'archive',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>