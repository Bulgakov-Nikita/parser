<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $data backend\modules\Parser\controllers\Compare сформированные даннные для вывода таблиц*/
/* @var $comp_data backend\modules\Parser\controllers\Compare сформированные даннные для вывода компетенций*/

$this->title = 'Сравнение планов';
?>

<?php
echo '<table border="1">';

echo '<tr>
        <th colspan="3" align="center">Исходная версия</th>
        <th colspan="3" align="center">Новая версия</th>
    </tr>
    <tr>
        <th style="width: 20%">Индекс</th>
        <th style="width: 20%">Наименование</th>
        <th style="width: 20%">Компетенции</th>
        <th style="width: 20%">Индекс</th>
        <th style="width: 20%">Наименование</th>
        <th style="width: 20%">Компетенции</th>
    </tr>';

$rows_count = count($data['index_1']);

for ($i = 0; $i < $rows_count; $i++) {
    echo '<tr>';

    // вывод исходной таблицы
    echo '<td>' . $data['index_1'][$i] . '</td>' .
        '<td>' . $data['name_1'][$i]  . '</td><td>';
    foreach ($data['comp_1'][$i] as $comp_name) {
        echo $comp_name . '; ';
    }
    echo '</td>';

    // вывод новой
    // if (array_key_exists($i, $data['is_equal']) && $data['is_equal'][$i] === true) {
    // if(array_key_exists($i, $data['is_equal'])){
        //вывод индекса
        if($data['is_equal'][$i]['index']){
            echo '<td>' . $data['index_2'][$i] . '</td>';
        }else{
            echo '<td bgcolor="#ffb6c1">' . $data['index_2'][$i] . '</td>';
        }

        //вывод названия дисциплины
        if($data['is_equal'][$i]['name']){
            echo '<td>' . $data['name_2'][$i]  . '</td>';
        }else{
            echo '<td bgcolor="#ffb6c1">' . $data['name_2'][$i]  . '</td>';
        }

        //вывод компетенций
        if($data['is_equal'][$i]['comp']){
            echo '<td>';
        }else{
            echo '<td bgcolor="#ffb6c1">';
        }
        foreach ($data['comp_2'][$i] as $comp_name) {
            echo $comp_name . '; ';
        }
        echo '</td>';

    echo '</tr>';
}

echo '</table>';
?>

<?php
echo '<br><h4>Сравнение компетенций</h4>';

echo '<table border="1">';

echo '<tr>
        <th colspan="2" align="center">Исходная версия</th>
        <th colspan="2" align="center">Новая версия</th>
    </tr>
    <tr>
        <th style="width: 20%">Индекс</th>
        <th style="width: 20%">Наименование</th>
        <th style="width: 20%">Индекс</th>
        <th style="width: 20%">Наименование</th>
    </tr>';

$rows_count = count($comp_data['index_1']);

for ($i = 0; $i < $rows_count; $i++) {
    echo '<tr>';

    // вывод исходной таблицы
    echo '<td>' . $comp_data['index_1'][$i] . '</td>' .
        '<td>' . $comp_data['name_1'][$i] . '</td>';

    // вывод новой
    if (array_key_exists($i, $comp_data['is_equal']) && $comp_data['is_equal'][$i] === true) {
        echo '<td>' . $comp_data['index_2'][$i] . '</td>' .
            '<td>' . $comp_data['name_2'][$i]  . '</td>';

    } else {
        echo '<td bgcolor="#ffb6c1">' . $comp_data['index_2'][$i] . '</td>' .
            '<td bgcolor="#ffb6c1">' . $comp_data['name_2'][$i]  . '</td>';
    }

    echo '</tr>';
}
echo '</table>';

?>
