<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use common\models\MainPlan;

/* @var $this yii\web\View */

$this->title = 'Выберите учебные планы для сравнения';
?>

<?php
$form = ActiveForm::begin([
    'id' => 'form-input-example',]);

$main_plans = array();
foreach (MainPlan::find()->all() as $mp){
    if ($mp->archive) {
        $main_plans[$mp->id] = $mp->strNamePlan() . ' (архив)';
    } else {
        $main_plans[$mp->id] = $mp->strNamePlan();
    }
}

echo \kartik\widgets\Select2::widget([
    'name' => 'main_plan_id_1',
    'data' => $main_plans,
    'options' => [
        'placeholder' => 'Исходный план',
        'multiple' => false,
        'style'=>'width:350px',
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => true,
        'width' => '25%',
    ],
]);

echo '<br>';

    echo \kartik\widgets\Select2::widget([
    'name' => 'main_plan_id_2',
    'data' => $main_plans,
    'options' => [
    'placeholder' => 'Новая версия плана',
    'multiple' => false,
    'style'=>'width:350px',
    ],
    'pluginOptions' => [
    'allowClear' => true,
    'multiple' => true,
    'width' => '25%',
    ],
    ]); ?>

    <br/>

    <div class="form-group">
        <?= Html::submitButton('Сравнить', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end() ?>