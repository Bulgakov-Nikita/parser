<?php

namespace backend\modules\Parser\controllers;

use backend\modules\Parser\models\search\MainPlanSearch;
use backend\models\tables\MainPlan;
use Yii;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\base\BaseObject;
use yii\filters\VerbFilter;
use yii\web\Controller;
use backend\modules\Parser\models\Parser;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
//                        'actions' => ['index', 'rooms', 'views'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {

        $searchModel = new MainPlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => new Parser(),
        ]);
    }

    public function actionLoadPlan(){
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new Parser();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model->file = UploadedFile::getInstance($model, 'file');
            if (!$model->check_extension()) {
                return array('error'=>true,'desc' => 'Ошибка типа файла');
            }
            $res = $model->parse();
            $model->upload();
            return $res;
        }
        else{
            throw new HttpException(510,'Ошибка переданных данных');
        }
    }

    public function actionView()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MainPlan::find(),

            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],

        ]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetails($id)
    {
        $model = $this->findModel($id);
        $providerCalendary = new \yii\data\ArrayDataProvider([
            'allModels' => $model->calendaries,
        ]);
        $providerComp = new \yii\data\ArrayDataProvider([
            'allModels' => $model->comps,
        ]);
        $providerDisciplins = new \yii\data\ArrayDataProvider([
            'allModels' => $model->disciplins,
        ]);
        $providerGmp = new \yii\data\ArrayDataProvider([
            'allModels' => $model->gmps,
        ]);
        $providerNid = new \yii\data\ArrayDataProvider([
            'allModels' => $model->ns,
        ]);
        $providerPodpisants = new \yii\data\ArrayDataProvider([
            'allModels' => $model->podpisants,
        ]);
        $providerSession = new \yii\data\ArrayDataProvider([
            'allModels' => $model->sessions,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerCalendary' => $providerCalendary,
            'providerComp' => $providerComp,
            'providerDisciplins' => $providerDisciplins,
            'providerGmp' => $providerGmp,
            'providerNid' => $providerNid,
            'providerPodpisants' => $providerPodpisants,
            'providerSession' => $providerSession,
        ]);
    }


    public function actionRooms()
    {
        return $this->redirect('__DIR__/');
    }

    /*public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }*/


    /**
     *
     * Export MainPlan information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerCalendary = new \yii\data\ArrayDataProvider([
            'allModels' => $model->calendaries,
        ]);
        $providerComp = new \yii\data\ArrayDataProvider([
            'allModels' => $model->comps,
        ]);
        $providerDisciplins = new \yii\data\ArrayDataProvider([
            'allModels' => $model->disciplins,
        ]);
        $providerGmp = new \yii\data\ArrayDataProvider([
            'allModels' => $model->gmps,
        ]);
        $providerNid = new \yii\data\ArrayDataProvider([
            'allModels' => $model->ns,
        ]);
        $providerPodpisants = new \yii\data\ArrayDataProvider([
            'allModels' => $model->podpisants,
        ]);
        $providerSession = new \yii\data\ArrayDataProvider([
            'allModels' => $model->sessions,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerCalendary' => $providerCalendary,
            'providerComp' => $providerComp,
            'providerDisciplins' => $providerDisciplins,
            'providerGmp' => $providerGmp,
            'providerNid' => $providerNid,
            'providerPodpisants' => $providerPodpisants,
            'providerSession' => $providerSession,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Finds the MainPlan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MainPlan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
