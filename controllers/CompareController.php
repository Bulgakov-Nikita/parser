<?php

namespace backend\modules\Parser\controllers;

use backend\modules\Parser\models\Compare;
use yii\filters\VerbFilter;
use yii\web\Controller;

class CompareController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['choice-main-plans'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    public function actionChoiceMainPlans() {
        if ($this->request->isPost) {
            $model = new Compare();
            $data = $model->compare(
                $this->request->post('main_plan_id_1'),
                $this->request->post('main_plan_id_2')
            );

            $comp_data = $model->get_comp_data(
                $this->request->post('main_plan_id_1'),
                $this->request->post('main_plan_id_2')
            );

            return $this->render('showPlans', [
                'data' => $data,
                'comp_data' => $comp_data,
            ]);
        }

        return $this->render('choiceMainPlans');
    }
}